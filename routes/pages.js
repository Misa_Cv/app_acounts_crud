const express = require('express');
const app = express()
const mongojs = require('mongojs')
const db = mongojs('milosdb',['acounts'])
const router = express.Router()



router.get('/',(req,res)=>{
    db.acounts.find((err,data)=>{
        res.render('index',{data:data})
    })

    
})

router.get('/add',(req,res)=>{
    res.render('add-view')
})
router.get('/edit',(req,res)=>{

    db.acounts.find((err,data)=>{
        res.render('edit-view',{data:data})
    })
})

router.get('/edit-acc/:id',(req,res)=>{
    let id = req.params.id
    db.acounts.findOne({"_id": db.ObjectId(id)},(err,data)=>{
        res.render('edit-acc',{data:data})
    })
})

module.exports=router

